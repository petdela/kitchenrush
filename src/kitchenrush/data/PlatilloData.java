/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import kitchenrush.modelo.Platillo;
import kitchenrush.modelo.Producto;
/**
 *
 * @author Municipio de Gye
 */
public class PlatilloData {
    public static void guardarPlatillos(ArrayList<Platillo> plat) throws IOException{
         try(ObjectOutputStream objOutputStream = new ObjectOutputStream(
                    new FileOutputStream("platillo.dat"))){
        objOutputStream.writeObject(plat);
        }
    }
    
    public static ArrayList<Platillo> leerPlatillo() throws ClassNotFoundException, IOException{
        ArrayList<Platillo> platillos=null;
        try(ObjectInputStream objInputStream = new ObjectInputStream(
                    new FileInputStream("platillo.dat"))){
        platillos= (ArrayList<Platillo>)objInputStream.readObject();
        }
        return platillos;
    }
    
    public static void crearArchivoIPlatillo(){
        ArrayList<Producto> productos=null;
        try {
            productos=ProductoData.leerProducto();
        } catch (ClassNotFoundException ex) {
            System.out.println("El archivo no fue encontrado");
        } catch (IOException ex) {
            System.out.println("Error desconocido por mortales");
        }
        ArrayList<Platillo> platilloL=new ArrayList<>();
        ArrayList<Producto> listHamburguesa= new ArrayList<>();
        ArrayList<Producto> listHotDog= new ArrayList<>();
        for (Producto p: productos){
            if((p.getNombre().equals("carne")) || (p.getNombre().equals("pan")) || 
                    (p.getNombre().equals("tomate")) || (p.getNombre().equals("cebolla"))){
                listHamburguesa.add(p);
            
            }if((p.getNombre().equals("panHD")) || (p.getNombre().equals("salchicha")) || 
                    (p.getNombre().equals("ruffles"))){
                listHotDog.add(p);
            }
        }
        platilloL.add(new Platillo("hamburguesa",listHamburguesa));
        platilloL.add(new Platillo("hot dog",listHotDog));
        try {
            //guardamos el archivo
            guardarPlatillos(platilloL);
        } catch (IOException ex) {
            System.out.println("Se produjo un error desconocido al guardar los productos");
        }
    }
    //Se agregan nuevos productos al archivo binario
    public static void AgregarPlatillo(Platillo plat){
        try {
            //Primero se lee el anterio archivo
            ArrayList<Platillo> platilloD=leerPlatillo();
            platilloD.add(plat);
            //Luego de añadir el producto a el arrayList se sobreesribe el archivo
            guardarPlatillos(platilloD);
        } catch (ClassNotFoundException ex) {
            System.out.println("No se encontro el archivo");
        } catch (IOException ex) {
            System.out.println("Error desconocido. Disculpe las molestias");
        }
    }
}
