/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import kitchenrush.modelo.Producto;

/**
 *
 * @author Municipio de Gye
 */
public class ProductoData {
    //Metodo para guardar un ArrayList de Producto
    public static void guardarProductos(ArrayList<Producto> productos) throws IOException{
        try(ObjectOutputStream objOutputStream = new ObjectOutputStream(
                    new FileOutputStream("producto.dat"))){
        objOutputStream.writeObject(productos);
        }
    }
    //Metodo para leer el archivo binario de producto
    public static ArrayList<Producto> leerProducto() throws ClassNotFoundException, IOException{
        ArrayList<Producto> prod=null;
        try(ObjectInputStream objInputStream = new ObjectInputStream(
                    new FileInputStream("producto.dat"))){
        prod= (ArrayList<Producto>)objInputStream.readObject();
        }
        return prod;
    }
    //Creamos el archivo inicial para el juego
    public static void crearArchivoIProducto(){
        ArrayList<Producto> produc=new ArrayList<>();
        produc.add(new Producto("pan"));
        produc.add(new Producto("cebolla"));
        produc.add(new Producto("carne"));
        produc.add(new Producto("tomate"));
        produc.add(new Producto("ruffles"));
        produc.add(new Producto("salchicha"));
        produc.add(new Producto("panHD"));
        try {
            //guardamos el archivo
            guardarProductos(produc);
        } catch (IOException ex) {
            System.out.println("Se produjo un error desconocido al guardar los productos");
        }
    }
    //Se agregan nuevos productos al archivo binario
    public static void AgregarProductos(Producto prod){
        try {
            //Primero se lee el anterio archivo
            ArrayList<Producto> productosD=leerProducto();
            productosD.add(prod);
            //Luego de añadir el producto a el arrayList se sobreesribe el archivo
            guardarProductos(productosD);
        } catch (ClassNotFoundException ex) {
            System.out.println("No se encontro el archivo");
        } catch (IOException ex) {
            System.out.println("Error desconocido. Disculpe las molestias");
        }
    }
}
