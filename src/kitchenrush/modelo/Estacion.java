/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush.modelo;

import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author pc
 */
public class Estacion {
    private ArrayList<Producto> productosRecogidos;
    private Platillo orden;
    private Rectangle estacion;
    
    public Estacion(){
        estacion=new Rectangle(148,74);
        estacion.setFill(Color.GREY);
        estacion.setStroke(Color.BLACK);
        productosRecogidos=new ArrayList<>();
        
    }
    public void fijarPosiciones(double x,double y){
        estacion.setX(x);
        estacion.setY(y);
    }
    public Node getOBjeto(){
        return estacion;
    }

    public ArrayList<Producto> getProductosRecogidos() {
        return productosRecogidos;
    }

    public void setProductosRecogidos(ArrayList<Producto> productosObtenidos) {
        this.productosRecogidos = productosObtenidos;
    }

    public void setOrden(Platillo orden) {
        this.orden = orden;
    }

    public Platillo getOrden() {
        return orden;
    }
    public void añadirProducR(Producto pr){
         productosRecogidos.add(pr);
    }
    public void vaciarLista(){
        productosRecogidos.clear();
    }
    
}
