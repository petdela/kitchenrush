/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kitchenrush.modelo;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import kitchenrush.data.CONSTANTES;

/**
 * 
 * @author Municipio Gye
 */
public class cocinero {
public ImageView chef;
public Producto prodcutoRec;
    
    public cocinero(){
        Image img = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"per2.png"),40,40,true,true);
        //agrega imagen al imageView
        chef = new ImageView(img);
    }
    //Metodo para fijar la posicion del producto
    public void fijarPosicionObjeto(double x, double y){
        //fija la poscion de x con respecto a X y Y usando 
        chef.setLayoutX(x);
        chef.setLayoutY(y);
    }
    
    public double getPosicionX(){
        return chef.getLayoutX();
    }
    
    public double getPosicionY(){
        return chef.getLayoutY();
    }
   //Metodo para obtener la imagen
    public Node getObjeto(){
        return chef;
    }

    public void setPersonaje(String ruta) {
        Image img = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+ruta));
        //agrega imagen al imageView
        this.chef = new ImageView(img);
    }

    public void setProdcutoRec(Producto prodcutoRec) {
        this.prodcutoRec = prodcutoRec;
    }

    public Producto getProdcutoRec() {
        return prodcutoRec;
    }
    
}