/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush.modelo;
import java.io.Serializable;
import java.util.Objects;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import kitchenrush.data.CONSTANTES;
/**
 *
 * @author Municipio de Gye
 */
public class Producto implements Serializable{
    public transient ImageView producto=null;
    public String nombre;
    public String pathAux="";
    public Producto() {
    }
    
    public Producto(String nombre){
        this.nombre=nombre;
        Image img = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+nombre+".png"),40,
                        40,
                        true,
                        true);
        //agrega imagen al imageView
        producto= new ImageView(img);
    }
    public void fijarPosicionObjeto(double x, double y){
        //fija la poscion de x con respecto a X y Y usando 
        producto.setLayoutX(x);
        producto.setLayoutY(y);
    }
    
    public double getPosicionX(){
        return producto.getLayoutX();
    }
    
    public double getPosicionY(){
        return producto.getLayoutY();
    }
    
    public Node getObjeto(){
        return producto;
    }  

    public String getNombre() {
        return nombre;
    }

    public void setProducto(ImageView producto) {
        this.producto = producto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ImageView getProducto() {
        return producto;
    }

    public String getPathAux() {
        return pathAux;
    }

    public void setPathAux(String pathAux) {
        this.pathAux = pathAux;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }
    public void setImagenAux(String ruta){
        Image img = new Image(ruta,40,
                        40,
                        true,
                        true);
        //agrega imagen al imageView
        producto= new ImageView(img);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
}

