/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Municipio de Gye
 */
public class Platillo implements Serializable{
    public String nombre;
    public ArrayList<Producto> produc;
    public Platillo() {
    }
    public Platillo(String nombre, ArrayList<Producto> produc) {
        this.nombre = nombre;
        this.produc = produc;
    }
    public String getNombre() {
        return nombre;
    }
    public ArrayList<Producto> getProduc() {
        return produc;
    }
    public void actualizarAray(Producto pr){
        produc.add(pr);
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setProduc(ArrayList<Producto> produc) {
        this.produc = produc;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.produc);
        return hash;
    }

}
