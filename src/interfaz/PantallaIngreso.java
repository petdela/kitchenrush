/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import kitchenrush.data.CONSTANTES;
import javafx.scene.canvas.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import static kitchenrush.data.ProductoData.*;
import static kitchenrush.data.PlatilloData.*;
import kitchenrush.modelo.Platillo;
import kitchenrush.modelo.Producto;
/**
 *
 * @author Petter De la Cruz
 */
public class PantallaIngreso {
    private VBox root;
    private Button botonRegresar;
    ObservableList<String> productos;
    TextField nombreIng;
    TextField platillo;
    String imagePath;
    ComboBox combo1;
    ComboBox combo2;
    ComboBox combo3;
    ComboBox combo4;
    ComboBox combo5;
            
    
    public PantallaIngreso(){
        //Inicializamos el contenedor raiz
        root = new VBox();
        //Agrega una imagen de fondo al contenedor
        BackgroundImage myBI= new BackgroundImage(new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"cocina2.png")), 
        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, 
        BackgroundSize.DEFAULT); 
        root.setBackground(new Background(myBI));
        crearSeccion1();
        crearSeccion2();
        crearSeccion3();
        crearSeccion4();
        crearSeccion5();
        crearSeccion6();
        crearSeccion7();
    }
    public void crearSeccion1(){
        Canvas canvas = new Canvas(700,90); //Se crea un canvas para escribi el titulo del juego y darle formato
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.YELLOW);
        gc.setStroke( Color.BLACK);
        gc.setLineWidth(2);
        Font theFont = Font.font("Rockwell Extra Bold", 36 );
        gc.setFont( theFont );
        gc.fillText( "Kitchen Rush!",180,60);
        gc.strokeText( "Kitchen Rush!",180,60);
        root.getChildren().add(canvas );
    }
    public void crearSeccion2(){
        GridPane gridPlabel=new GridPane(); //Se crea un GridPane para luego de añadir un Label setearle los espacios
        Label AgregarP=new Label("Agregar Producto:");
        AgregarP.setFont(Font.font("Rockwell Extra Bold", 22 ));
        gridPlabel.add(AgregarP,1,1);
        gridPlabel.setHgap(100); //espacio horizontal de gridPane
        gridPlabel.setVgap(20); //espacio vertical de gridPane
        root.getChildren().add(gridPlabel);
    }   
    public void crearSeccion3(){
        GridPane gridP=new GridPane();
        Label nombre=new Label("Nombre Producto:");
        nombre.setFont(Font.font("Rockwell Extra Bold", 16 ));
        nombre.setStyle("-fx-text-fill: blue;");
        gridP.add(nombre,1,1);
        nombreIng= new TextField();
        nombreIng.setPrefWidth(90); //Se modifica el ancho del TextField
        gridP.add(nombreIng,2,1);
        Button botonSImagen = new Button("Subir imagen"); //Se crea el boton para subir la imagen
        botonSImagen.setPrefSize(100,30);         //Tamaño del boton
        ObtenerImagen(botonSImagen);
        gridP.add(botonSImagen,3,1);
        Button botonAceptar = new Button("Aceptar"); //Se crea el boton para aceptar
        AceptarPro(botonAceptar);
        botonAceptar.setPrefSize(100,30);        //Tamaño del boton
        gridP.add(botonAceptar,4,1);
        gridP.setHgap(20); //espacio horizontal de gridPane
        gridP.setVgap(20); //espacio vertical de gridPane
        root.getChildren().add(gridP);
    }
    public void crearSeccion4(){
        GridPane gridPlabel2=new GridPane(); //Se crea un GridPane para luego de añadir un Label setearle los espacios
        Label AgregarPla=new Label("Agregar Platillo:");
        AgregarPla.setFont(Font.font("Rockwell Extra Bold", 22 ));
        gridPlabel2.add(AgregarPla,1,1);
        gridPlabel2.setHgap(100); //espacio horizontal de gridPane
        gridPlabel2.setVgap(10); //espacio vertical de gridPane
        root.getChildren().add(gridPlabel2);
    }
    public void crearSeccion5(){
        GridPane gridP2=new GridPane();
        Label nombrePlat=new Label("Nombre Platillo:");
        nombrePlat.setFont(Font.font("Rockwell Extra Bold", 16 ));
        nombrePlat.setStyle("-fx-text-fill: blue;");
        gridP2.add(nombrePlat,1,1);
        platillo= new TextField();
        platillo.setPrefWidth(90); //Se modifica el ancho del TextField
        gridP2.add(platillo,2,1);
        gridP2.setHgap(20); //espacio horizontal de gridPane
        gridP2.setVgap(20); //espacio vertical de gridPane
        root.getChildren().add(gridP2);
    }
    public void crearSeccion6(){
        GridPane gridP3=new GridPane();
        crearOBservable();
        combo1 = new ComboBox<>(productos);
        combo2 = new ComboBox<>(productos);
        combo3 = new ComboBox<>(productos);
        combo4 = new ComboBox<>(productos);
        combo5 = new ComboBox<>(productos);
        gridP3.add(combo1,1,1);
        gridP3.add(combo2,2,1);
        gridP3.add(combo3,3,1);
        gridP3.add(combo4,4,1);
        gridP3.add(combo5,5,1);
        gridP3.setHgap(20); //espacio horizontal de gridPane
        gridP3.setVgap(20); //espacio vertical de gridPane
        root.getChildren().add(gridP3);
    }
    public void crearSeccion7(){
        GridPane gridP4=new GridPane();
        Button botonAceptar = new Button("Aceptar"); //Se crea el boton para Aceptar
        AceptarPlatl(botonAceptar);
        botonAceptar.setPrefSize(100,30);         //Tamaño del boton
        gridP4.add(botonAceptar,1,1);
        botonRegresar=new Button("Regresar Menu"); //Se crea el boton para Salir
        botonRegresar.setPrefSize(100,30);        //Tamaño del boton
        gridP4.add(botonRegresar,2,1);
        gridP4.setHgap(20); //espacio horizontal de gridPane
        gridP4.setVgap(20); //espacio vertical de gridPane
        root.getChildren().add(gridP4);
    }
    //Gracias a este metodo obtenemos la ruta de la imagen buscada por el fileChooser
    public void ObtenerImagen(Button bt){
        //Seteamos la accion al boton
        bt.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
            //Se crea un objeto filecchooser para en base a el escoger una imagen
            FileChooser jfc=new FileChooser();
            //Se escoge filtro para solo escoger imagenes
            jfc.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
            //Obtenemos el archivo de la opcion elegida
            File opcion=jfc.showOpenDialog(null);
            if(opcion!=null){
                try {
                    //Coreccion de error con la URL de la imagen para ser usada
                    imagePath = opcion.toURI().toURL().toString();
                } catch (MalformedURLException ex) {
                    System.out.println("Error en obtencion de imagen. Se intentara solucionar ");
                }
                }
            }
        });  
    }
    public void AceptarPro(Button btn){
        //Seteamos la opcion de aceptar el ingreso del producto
        btn.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                //Obtenemos el contenido del TextField
                String nombrePro=nombreIng.getText();
                //Limpiamos el TextField
                nombreIng.clear();
                //Creamos un nuevo producto y le agregamos la rutade la imagen deseada
                Producto productoNew=new Producto();
                productoNew.setNombre(nombrePro);
                productoNew.setPathAux(imagePath);
                AgregarProductos(productoNew);
                //Nos aseguramos de actualzar el observableList
                crearOBservable();
                //Actualizamos los comboBox
                combo1.setItems(productos);
                combo2.setItems(productos);
                combo3.setItems(productos);
                combo4.setItems(productos);
                combo5.setItems(productos);
            }
        }); 
    }
    public void AceptarPlatl(Button btn){
        //Seteamos la opcion de aceptar el ingreso del platillo
        btn.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                try {
                    //Obtenemos el contenido del TextField
                    String nombrePlat=platillo.getText();
                    //Limpiamos el TextField
                    platillo.clear();
                    //Obtenemos los productos del Combobox
                    Platillo platillonew= new Platillo();
                    ArrayList<String> nombres=new ArrayList();
                    nombres.add((String)combo1.getValue());
                    nombres.add((String)combo2.getValue());
                    nombres.add((String)combo3.getValue());
                    nombres.add((String)combo4.getValue());
                    nombres.add((String)combo5.getValue());
                    ArrayList<Producto> productLis=leerProducto();
                    if(nombres.size()>=3){
                    boolean permitir=true;
                    int contador=0;
                    for(String n:nombres){
                        for(Producto pr: productLis){
                            if(n!=null && n.equals(pr.getNombre())){
                                platillonew.actualizarAray(pr);
                            }
                        }
                        if(contador==4){
                            permitir=false;
                        }if(n==null){
                        contador++;
                        }
                    }
                    platillonew.setNombre(nombrePlat);
                    if(platillonew.getNombre()!=null && permitir){
                        AgregarPlatillo(platillonew);
                    }else{
                        System.out.println("Errorororo");
                    }
                    }
                } catch (ClassNotFoundException ex) {
                    System.out.println("Clase no encontrada");
                } catch (IOException ex) {
                    System.out.println("Error general");
                }
               
            }
        }); 
    }
    public void crearOBservable(){
        // Creacion de los datos para mostrar 
        productos = FXCollections.observableArrayList();
        try {
            ArrayList<Producto> productos2=leerProducto();
            for(Producto pr: productos2){
                productos.add(pr.getNombre());            
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("No se ha podido encontrar el archivo");
        } catch (IOException ex) {
            System.out.println("Error que esta fuera del alcance de mi conocimiento ha ocurrido. Lo siento");
        }
    }
    public VBox getRoot(){
        return root;
    }
    public Button getBotonRegresar() {
        return botonRegresar;
    }
}

