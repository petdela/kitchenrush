/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.l
 */
package interfaz;

import java.io.File;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import kitchenrush.data.CONSTANTES;
import static kitchenrush.data.PlatilloData.crearArchivoIPlatillo;
import static kitchenrush.data.ProductoData.*;

/**
 *
 * @author 
 */
public class Pantalla extends Application {
    //public static Scene escena;
    @Override
    public void start(Stage primaryStage) {
        //escena = new Scene(paI.getRoot(), 700, 700);
        
        PantallaInicial paI = new PantallaInicial();
        PantallaIngreso paIng=new PantallaIngreso();
        PantallaJuego paJ=new PantallaJuego();
 
        Scene scene = new Scene(paI.getRoot(),CONSTANTES.ANCHOJUEGO,CONSTANTES.ALTOJUEGO);
        Scene scenenew = new Scene(paIng.getRoot(),CONSTANTES.ANCHOJUEGO,CONSTANTES.ALTOSEGUNDAP);
        Scene scenenew2 =new Scene(paJ.getRoot(),900,700);
        //Se setean las acciones de los botones para cambair las escenas y salir del juego
        paI.getBotonAgregarI().setOnAction((ActionEvent e)->{
            primaryStage.setScene(scenenew);
        });
        paI.getBotonSalir().setOnAction((ActionEvent e)->{
            Platform.exit();
        });
        paIng.getBotonRegresar().setOnAction((ActionEvent e)->{
            primaryStage.setScene(scene);
        });
        paI.getBotonIniciar().setOnAction((ActionEvent e)->{
            primaryStage.setScene(scenenew2);
        });
        primaryStage.setTitle("Kitchen Rush! Game Play");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Creacion del archivo inicial para las pruebas
        File archivoI=new File("producto.dat");
        if(archivoI.exists()==false){
            crearArchivoIProducto();
        }
        File archivoI2=new File("platillo.dat");
        if(archivoI2.exists()==false){
            crearArchivoIPlatillo();
        }
        launch(args);
    }
    
}
