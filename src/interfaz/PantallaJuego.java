
package interfaz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import kitchenrush.data.CONSTANTES;
import static kitchenrush.data.PlatilloData.leerPlatillo;
import static kitchenrush.data.ProductoData.leerProducto;
import kitchenrush.modelo.Estacion;
import kitchenrush.modelo.Platillo;
import kitchenrush.modelo.Producto;
import kitchenrush.modelo.cocinero;

/**
 *
 * @author moises
 */
public class PantallaJuego {
     private BorderPane root;
     private Label marcador1;
     private Label marcador2;
     private Label tiempo;
     private int puntosJ1=0;
     private int puntosJ2=0;
     private double tiempoTranscurrido;
     private boolean terminarJuego;
     private Pane gamePane;
     private AnchorPane banner;
     private cocinero chef;
     private cocinero chef2;
     private Random random;
     private ArrayList<Producto> productosPorRecolectar = new ArrayList<>();
     private ArrayList<Node> obstaculos=new ArrayList<>();
     private Estacion estacion1=new Estacion();
     private Estacion estacion2=new Estacion();
     private Platillo orden1;
     private Platillo orden2;
     private BorderPane leftJ1;
     private BorderPane rightJ2;
     private VBox datosProductos1=new VBox();
     private VBox datosProductos2=new VBox();
     private Button pausar;
    

    public PantallaJuego(){
        random = new Random();
        root =new  BorderPane();
        
        root.setTop(seccionSuperior());
        root.setCenter(seccionCentral());
        root.setLeft(seccionIzquierda());
        root.setRight(seccionDerecha());
        crearObstaculo();
        crearEstaciones();
        /*crearImagenProducto();
        orden=generaOrden();
        estacion1.setOrden(orden);
        estacion2.setOrden(orden);*/
        avanzarJuego();
        
    }
    public AnchorPane seccionSuperior(){
        //Creacion de un GridPane para posicionar elementos 
        banner=new AnchorPane();
        banner.setMinSize(900,100);
        //Creacion de un objeto imagen que se ubicara en la pantalla de inicio
        banner.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_RECURSOS+"/banner.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+900+" "+(100)+"; "
                + "-fx-background-position: center center;");
        Font theFont =Font.font("Sitka Small",FontWeight.BLACK,30);
        pausar=new Button("Pausar");
        Label lt = new Label("Tiempo:");
        lt.setFont(theFont);
        tiempo = new Label("00:00");
        tiempo.setFont(theFont);
        lt.setTextFill(Color.WHITE);
        tiempo.setTextFill(Color.WHITE);
        banner.getChildren().addAll(lt,tiempo,pausar);
 
        AnchorPane.setRightAnchor(lt, 350.00);
        AnchorPane.setRightAnchor(tiempo, 350.00);
        AnchorPane.setRightAnchor(pausar, 200.00);
        AnchorPane.setTopAnchor(lt, 40.00);
        AnchorPane.setTopAnchor(tiempo , 70.00);
        AnchorPane.setTopAnchor(pausar , 70.00);

        return banner;

        //Scene scene2 = new Scene(root2,300,300);
      
    }
    public Pane seccionCentral(){
        //Creacion de un GridPane para posicionar elementos 
        gamePane =new Pane();
        chef=new cocinero();
        chef2=new cocinero();
        chef2.setPersonaje("per1.png");
        
        //Creacion de un objeto imagen que se ubicara en la pantalla de Juego
        gamePane.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_RECURSOS+"/pisoFondo.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+600+" "+(600)+"; "
                + "-fx-background-position: center center;");
        gamePane.getChildren().add(chef.getObjeto());
        gamePane.getChildren().add(chef2.getObjeto());
        double x= 350;
        double y=180;
       
        double x2= 400;
        double y2=180;
        chef.fijarPosicionObjeto(x, y);
        chef2.fijarPosicionObjeto(x2, y2);

        return gamePane;  
        
    }
    public BorderPane seccionIzquierda(){
         VBox datos=new VBox();
         leftJ1=new BorderPane();
         leftJ1.setMinSize(150,600);
        //Creacion de un objeto imagen que se ubicara en la pantalla de inicio
        leftJ1.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_RECURSOS+"/borde.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+150+" "+(600)+"; "
                + "-fx-background-position: center center;");
        Font theFont =Font.font("Sitka Small",FontWeight.BLACK,20);
        
        marcador1=new Label(String.valueOf(puntosJ1));
        marcador1.setFont(theFont);
        
        Label lp=new Label("Puntos del");
        Label lj=new Label("Jugador 1");
        lp.setFont(theFont);
        lp.setTextFill(Color.WHITE);
        lj.setTextFill(Color.WHITE);
        lj.setFont(theFont);
        datos.getChildren().addAll(marcador1,lp,lj);
        datos.setSpacing(5);
        datos.setAlignment(Pos.CENTER);
        leftJ1.setBottom(datos);
 
        return leftJ1;
      
    }
    public BorderPane seccionDerecha(){
         rightJ2=new BorderPane();
         VBox datos=new VBox();
         rightJ2.setMinSize(150, 600);
        //Creacion de un objeto imagen que se ubicara en la pantalla de inicio
        rightJ2.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_RECURSOS+"/borde.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+150+" "+(600)+"; "
                + "-fx-background-position: center center;");
        marcador2=new Label(String.valueOf(puntosJ2));
        Font theFont =Font.font("Sitka Small",FontWeight.BLACK,20);
        marcador2.setFont(theFont);

        Label lp=new Label("Puntos del");
        Label lj=new Label("Jugador 2");
        lp.setTextFill(Color.WHITE);
        lj.setTextFill(Color.WHITE);
        lp.setFont(theFont);
        lj.setFont(theFont);
        datos.getChildren().addAll(marcador2,lp,lj);
        datos.setSpacing(5);
        datos.setAlignment(Pos.CENTER);
        rightJ2.setBottom(datos);
        return rightJ2;
    }
    
    //Se diseña un metodo que movera al personaje de acuerdo a lo ingresado por teclado
    public void avanzarJuego(){
        
        gamePane.setFocusTraversable(true); 
        //Los condicionales sirven para que el personaje no pase los bordes
        gamePane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case ENTER: 
                        //Creación del hilo que controla el cronómetro
       
                        HilodelTiempo hdt = new HilodelTiempo();
                        Thread t1 = new Thread(hdt); 
                        GenerarPlatillo gp=new GenerarPlatillo();
                        Thread t2=new Thread(gp);
                            
                        //Para que el hilo corra tengo que llamar al método start
                        t1.start();
                        t2.start();
                        break;
                    case SPACE: 
                        //falta
                        break;
                   
                }
            }
        });
    }
    
    public void moverCocinero(){
        
        gamePane.setFocusTraversable(true); 
        //Los condicionales sirven para que el personaje no pase los bordes
        gamePane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case W: 
                        if(chef.getObjeto().getLayoutY()>10){
                        //mover arriba
                        //nuevay = posiciony - delta;
                        double y1 = chef.getObjeto().getLayoutY() - 10;
                        chef.getObjeto().setLayoutY(y1);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double y1 = chef.getObjeto().getLayoutY()+ 10;
                            chef.getObjeto().setLayoutY(y1);
                        }
                        break;
                    case S: 
                        //mover abajo
                        //nuevay = posiciony + delta;
                        if(chef.getObjeto().getLayoutY()<550){
                        double y2 = chef.getObjeto().getLayoutY() + 10;
                        chef.getObjeto().setLayoutY(y2);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double y2 = chef.getObjeto().getLayoutY()- 10;
                            chef.getObjeto().setLayoutY(y2);
                        }
                        break;
                    case A: 
                        //mover a la izquierda
                        //nuevax = posiciony + delta;
                        if(chef.getObjeto().getLayoutX()>10){
                        double x1 = chef.getObjeto().getLayoutX() - 10;
                        chef.getObjeto().setLayoutX(x1);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double x1 = chef.getObjeto().getLayoutX()+ 10;
                            chef.getObjeto().setLayoutX(x1);
                        }
                        break;
                    case D: 
                        //mover a la darecha
                        //nuevax = posiciony - delta;
                        if(chef.getObjeto().getLayoutX()<550){
                        double x2 = chef.getObjeto().getLayoutX() + 10;
                        chef.getObjeto().setLayoutX(x2);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double x2 = chef.getObjeto().getLayoutX()- 10;
                            chef.getObjeto().setLayoutX(x2);
                        }
                        break;
                        case UP:
                        event.consume();
                        if(chef2.getObjeto().getLayoutY()>10){
                        //mover arriba
                        //nuevay = posiciony - delta;
                        double y1 = chef2.getObjeto().getLayoutY() - 10;
                        chef2.getObjeto().setLayoutY(y1);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double y1 = chef2.getObjeto().getLayoutY()+ 10;
                            chef2.getObjeto().setLayoutY(y1);
                        }
                        break;
                    case DOWN: 
                        //mover abajo
                        //nuevay = posiciony + delta;
                        if(chef2.getObjeto().getLayoutY()<550){
                        double y2 = chef2.getObjeto().getLayoutY() + 10;
                        chef2.getObjeto().setLayoutY(y2);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double y2 = chef2.getObjeto().getLayoutY()- 10;
                            chef2.getObjeto().setLayoutY(y2);
                        }
                        break;
                    case LEFT: 
                        //mover a la izquierda
                        //nuevax = posiciony + delta;
                        if(chef2.getObjeto().getLayoutX()>10){
                        double x1 = chef2.getObjeto().getLayoutX() - 10;
                        chef2.getObjeto().setLayoutX(x1);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double x1 = chef2.getObjeto().getLayoutX()+ 10;
                            chef2.getObjeto().setLayoutX(x1);
                        }
                        break;
                    case RIGHT: 
                        //mover a la darecha
                        //nuevax = posiciony - delta;
                        if(chef2.getObjeto().getLayoutX()<550){
                        double x2 = chef2.getObjeto().getLayoutX() + 10;
                        chef2.getObjeto().setLayoutX(x2);
                        }
                        chequearColisiones();
                        if(chequearColisionesObs()){
                            double x2 = chef2.getObjeto().getLayoutX()- 10;
                            chef2.getObjeto().setLayoutX(x2);
                        }
                        break;
                } 
                                                                   
            }
        });
    }
    
    //se crean los obstáculos en el juego
    public Platillo crearPlatillo(){
        Random rd=new Random();
        ArrayList<Platillo> listaPla=null;
         try {
             listaPla=leerPlatillo();
         } catch (ClassNotFoundException ex) {
             System.out.println("Archivo no encontrado. Sorry");
         } catch (IOException ex) {
             System.out.println("Error desconocido ha aparecido. Lo lamentamos");
         }
         int cantidad=listaPla.size();
         int indiceAl=rd.nextInt(cantidad);
         return listaPla.get(indiceAl);
    }
    
    private void crearObstaculo(){
            List<String> anchos=Arrays.asList("222","148");
            List<String> largos=Arrays.asList("74","74");
            List<String> pX=Arrays.asList("76","226");
            List<String> pY=Arrays.asList("148","296");
            Rectangle obstaculo;
            for(int i=0;i<anchos.size();i++){
                int ancho=Integer.parseInt(anchos.get(i));
                int largo=Integer.parseInt(largos.get(i));
                int x=Integer.parseInt(pX.get(i));
                int y=Integer.parseInt(pY.get(i));
                obstaculo=new Rectangle(ancho,largo);
                obstaculo.setFill(Color.BROWN);
                obstaculo.setStroke(Color.BLACK);
                obstaculo.setX(x);
                obstaculo.setY(y);
                obstaculos.add(obstaculo);
                gamePane.getChildren().add(obstaculo);
                
            }
            
           
      
    }
    //se crean las estaciones que recogen los productos
    private void crearEstaciones(){
            estacion1.fijarPosiciones(0,0);
            estacion2.fijarPosiciones(450, 522);
            obstaculos.add(estacion1.getOBjeto());
            obstaculos.add(estacion2.getOBjeto());
            gamePane.getChildren().addAll(estacion1.getOBjeto(),estacion2.getOBjeto());
    }
    //Se crea un metodo que cree la imagen de un Producto
    private void crearImagenProducto(Platillo platillo){
        
        ArrayList<Producto> pro=platillo.getProduc();
        for (Producto pr:pro){
            Point2D pos= generarUbicacion(40,40);
            Producto producto=null;
            if(pr.getPathAux()==""){
                producto=new Producto();
                producto.setImagenAux(pr.getPathAux());
            }else{
                producto= new Producto(pr.getNombre());
            }
            producto.fijarPosicionObjeto(pos.getX(),pos.getY());
            for(Node n:obstaculos){
                boolean condicion=true;
                while(condicion){
                    if(isCollision(n,producto.getObjeto() )|| isCollision(producto.getObjeto(),chef.getObjeto())|| isCollision(producto.getObjeto(),chef2.getObjeto())){
                        Point2D nPos=generarUbicacion(40,40);
                        producto.fijarPosicionObjeto(nPos.getX(),nPos.getY());
                    }else{
                        condicion=false;
                    }

                    }
            
            }
            productosPorRecolectar.add(producto);
            gamePane.getChildren().add(producto.getObjeto());
            
            
        }
            
    }   
    
    private class GenerarPlatillo implements Runnable{

        @Override
        public void run() {
            
            while(!terminarJuego){
                
                Platform.runLater(()->{
                    ArrayList<Platillo>ordenes=new ArrayList<>();
                    ArrayList<VBox>roots=new ArrayList<>();
                    ArrayList<BorderPane>costados=new ArrayList<>();
                    roots.add(datosProductos1);
                    roots.add(datosProductos2);
                    costados.add(leftJ1);
                    costados.add(rightJ2);
                    Platillo ordenA=crearPlatillo();
                    Platillo ordenB=crearPlatillo();
                    crearImagenProducto(ordenA);
                    crearImagenProducto(ordenB);
                    ordenes.add(ordenA);
                    ordenes.add(ordenB);
                    estacion1.setOrden(ordenA);
                    estacion2.setOrden(ordenB);
                    Font theFont =Font.font("Sitka Small",FontWeight.BLACK,15);
                    Font theFont2 =Font.font("Sitka Small",FontWeight.EXTRA_BOLD,20);

                    Label nombreO=new Label("Platillo:");
                    nombreO.setFont(theFont2);
                    nombreO.setTextFill(Color.WHITE);
                    Label nombreO2=new Label("Platillo:");
                    nombreO2.setFont(theFont2);
                    nombreO2.setTextFill(Color.WHITE);
                    Label orden1=new Label(ordenA.getNombre());
                    orden1.setFont(theFont2);
                    orden1.setTextFill(Color.WHITE);
                    Label orden2=new Label(ordenB.getNombre());
                    orden2.setFont(theFont2);
                    orden2.setTextFill(Color.WHITE);
                    datosProductos1.getChildren().addAll(nombreO,orden1);
                    datosProductos2.getChildren().addAll(nombreO2,orden2);
                    for(int i=0;i<2;i++){
                            VBox columnaP=new VBox();
                        
                        for(Producto produc:ordenes.get(i).getProduc()){
                            HBox fila=new HBox();
                            Label nombre=new Label(produc.getNombre());
                            Node imagen =produc.getObjeto();
                            nombre.setFont(theFont);
                            fila.getChildren().add(nombre);
                            columnaP.getChildren().add(fila);
                            
                        }
                        roots.get(i).getChildren().add(columnaP);
                        costados.get(i).setCenter(roots.get(i));
                        
                    
                    }
                    
                    
                });
                
                
                try{
                    Thread.sleep(30000);
                    estacion1.vaciarLista();
                    estacion2.vaciarLista();
                }catch (InterruptedException ex) {
                    Logger.getLogger(PantallaJuego.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    //Metodo que genera una posicion
    private Point2D generarUbicacion(double xx,double yy){
        double maximox=CONSTANTES.ANCHOPJUEGO-xx;
        double maximoy=CONSTANTES.ALTOPJUEGO-yy;
        double x= random.nextDouble()*maximox;
        double y= random.nextDouble()*maximoy;
        
        return new Point2D(x,y);
    
    }
    
    private class HilodelTiempo implements Runnable{
        @Override
        public void run() {
            while (!terminarJuego){
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PantallaJuego.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Error");
                }
                tiempoTranscurrido += 0.5;
                System.out.println(tiempoTranscurrido);
                Platform.runLater(()->{
                    //modifica el label del tiempo
                    tiempo.setText(Double.toString(tiempoTranscurrido));
                    moverCocinero();
                    //moverCocinero2();
                });
                
                if (tiempoTranscurrido>= 65){
                    terminarJuego = true;
                }
                
               
            }
        }
        
    }
    
    
    
    public static boolean isCollision(Node n1, Node n2){
        Bounds b1 = n1.getBoundsInParent();
        Bounds b2 = n2.getBoundsInParent();
        if(b1.intersects(b2)){
            return true;
        }else{
            return false;
        }
    }
    
    private void chequearColisiones(){
        Iterator<Producto> it= productosPorRecolectar.iterator();
        while(it.hasNext()){
            Producto p=it.next();
            if(isCollision(p.getObjeto(),chef.getObjeto()) ){
                if(chef.getProdcutoRec()==null){
                chef.setProdcutoRec(p);
                gamePane.getChildren().remove(p.getObjeto());
                }
                
                
            }else if(isCollision(p.getObjeto(),chef2.getObjeto())){
                if(chef2.getProdcutoRec()==null){
                chef2.setProdcutoRec(p);
                gamePane.getChildren().remove(p.getObjeto());
                }
            }
        }
    }
    //Devuelve un booleano en caso de que se haya chocado o no
    private boolean chequearColisionesObs(){
        Iterator<Node>it= obstaculos.iterator();
        while(it.hasNext()){
            Node n=it.next();
            if(isCollision(n,chef.getObjeto()) || isCollision(n,chef2.getObjeto())){
                if(isCollision(estacion1.getOBjeto(),chef.getObjeto())){
            if(chef.getProdcutoRec()!=null){
               Platillo plat=estacion1.getOrden();
               ArrayList<Producto> prod=plat.getProduc();
               if(!estacion1.getProductosRecogidos().contains(chef.getProdcutoRec())){
               for(Producto pr: prod){
                   if(pr.equals(chef.getProdcutoRec())){
                       estacion1.añadirProducR(chef.getProdcutoRec());
                       break;
                   }
               }
               chef.setProdcutoRec(null);
               }else{
                   chef.setProdcutoRec(null);
               }
            }
        }
                if(isCollision(estacion2.getOBjeto(),chef2.getObjeto())){
            if(chef2.getProdcutoRec()!=null){
               Platillo plat=estacion2.getOrden();
               ArrayList<Producto> prod=plat.getProduc();
               boolean dec=estacion2.getProductosRecogidos().contains(chef2.getProdcutoRec());
               if(!dec){
               for(Producto pr: prod){
                   if(pr.equals(chef2.getProdcutoRec())){
                       estacion2.añadirProducR(chef2.getProdcutoRec());
                       break;
                   }
               }
               chef2.setProdcutoRec(null);
               }else{
                   chef2.setProdcutoRec(null);
               }
            }
        }
                return true;
            }
        }
        return false;
    }
public void Pausar(Button btn){
    //Seteamos la opcion de aceptar el ingreso del producto
        btn.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent e){
                
            }
        }); 
    }
    public BorderPane getRoot() {
        return root;
    }

    
}
