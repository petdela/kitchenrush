/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 *
 * @author Municipio de Gye
 */
public class PantallPausa {
    private VBox root;
    Button seguir;
    Button regresar;
    public PantallPausa(){
        seguir=new Button("Continuar");
        regresar=new Button("Regresar Menu");
        root.getChildren().addAll(seguir,regresar);
        root.setPadding(new Insets(15));
        
    }
    public Button getSeguir() {
        return seguir;
    }
    public Button getRegresar() {
        return regresar;
    }
        public VBox getRoot() {
        return root;
    }
}
