/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;
import kitchenrush.data.CONSTANTES;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
/**
 *
 * @author Petter De la Cruz
 */
public class PantallaInicial {
    private VBox root;
    private Button botonAgregarI;
    private Button botonIniciar;
    private Button botonSalir ;
    
    public PantallaInicial(){
        //Inicializamos el contenedor raiz
        root = new VBox();
        crearSeccionS();
        crearSeccionI();
    }
    
    public void crearSeccionS(){
        //Creacion de un Canvas (Anteriormente se lo tenia pensado para escribir 
        //el titulo de forma manual) contenedor superior
        Canvas canvas = new Canvas(700,200);
        root.getChildren().add(canvas);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Image logo = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"logo3.png"));
        gc.drawImage(logo,50,0);
    }
    public void crearSeccionI(){
        //Creacion de un GridPane para posicionar elementos 
        GridPane inferior=new GridPane();
        //Creacion de un objeto imagen que se ubicara en la pantalla de inicio
        Image fondoP1 = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"gordon ramsay3.png"));
        ImageView persona1=new ImageView(fondoP1);
        //Establecimiento de la posicion de la imagen
        inferior.add(persona1,0,200);
        //Creacion del VBox que contendra los 2 botones
        VBox central=new VBox();
        //Creacion del boton
        botonIniciar = new Button("Iniciar juego");
        //Tamaño del boton
        botonIniciar.setPrefSize(250,125);
        //Creacion del boton
        botonAgregarI = new Button("Agregar Ingredientes"+"\n"+" y platillos");
        //Tamaño del boton agregar ingredientes
        botonAgregarI.setPrefSize(250,125);
        //Creacion del boton salir
        botonSalir = new Button("Salir");
        //Tamaño del boton
        botonSalir.setPrefSize(250,125);
        //Un Label que reconoce que el grupo7 creo el juego
        Label nombreGa= new Label("                    ¡¡Compañia G7POO!!");
        //Se añaden los botonos y el label
        central.getChildren().addAll(botonIniciar,botonAgregarI,botonSalir,nombreGa);
        //fijamos espacio entre los elementos
        central.setSpacing(25);
        //Se añade al Grid Pane el HBox
        inferior.add(central,200,200);
        //Creacion de un objeto imagen que se ubicara en la pantalla de inicio
        Image fondoP2 = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"ratatuille3.png"));
        ImageView persona2=new ImageView(fondoP2);
        //Establecimiento de la posicion de la imagen
        inferior.add(persona2,400,200);
        //Se añade al contenedor raiz
        root.getChildren().add(inferior);
    }
    public VBox getRoot(){
        return root;
    }
    public Button getBotonAgregarI() {
        return botonAgregarI;
    }
    public Button getBotonIniciar() {
        return botonIniciar;
    }
    public Button getBotonSalir() {
        return botonSalir;
    }
}