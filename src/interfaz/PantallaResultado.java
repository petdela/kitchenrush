/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import kitchenrush.data.CONSTANTES;
import kitchenrush.modelo.Platillo;

/**
 *
 * @author Municipio de Gye
 */
public class PantallaResultado {
    private HBox root;
    public PantallaResultado(double scorePJ1, double scorePJ2, ArrayList<Platillo> 
            ordenes1,ArrayList<Platillo> ordenes2 ){
        root=new HBox();   
        crearResultadosPJ2(scorePJ1,ordenes1);
        crearResultadosPJ2(scorePJ2, ordenes2);
    }
    public void crearResultadosPJ1(double score,ArrayList<Platillo> 
            ordenes1){
        VBox resultPj1=new VBox();
        Image img = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"per1.png"),40,40,true,true);
        ImageView PJ1= new ImageView(img);
        resultPj1.getChildren().add(PJ1);
        Label textl=new Label("Resultados PJ1");
        resultPj1.getChildren().add(textl);
        Label result=new Label(score+"");
        resultPj1.getChildren().add(result);
        for(Platillo pl: ordenes1){
            Label platillo=new Label(pl.getNombre());
            resultPj1.getChildren().add(platillo);
        }
        root.getChildren().add(resultPj1);
    }
    public void crearResultadosPJ2(double score,ArrayList<Platillo> 
            ordenes2){
        VBox resultPj2=new VBox();
        Image img = new Image(getClass().getResourceAsStream(
                    CONSTANTES.RUTA_RECURSOS+"/"+"per2.png"),40,40,true,true);
        ImageView PJ2= new ImageView(img);
        resultPj2.getChildren().add(PJ2);
        Label textl=new Label("Resultados PJ2");
        resultPj2.getChildren().add(textl);
        Label result=new Label(score+"");
        resultPj2.getChildren().add(result);
        for(Platillo pl: ordenes2){
            Label platillo=new Label(pl.getNombre());
            resultPj2.getChildren().add(platillo);
        }
        root.getChildren().add(resultPj2);
    }
    public HBox getRoot() {
        return root;
    }
    
}
